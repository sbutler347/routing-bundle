<?php
namespace DL\RoutingBundle\Routing;

use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\HttpKernel\Log\LoggerInterface;

class ExtraLoader extends Loader
{
    private $loaded = false;
    private $appRootDir;
    private $sites;

    /**
     * @param string $appRootDir base folder for the site
     * @param array $sites
     */
    public function __construct($appRootDir, $sites, LoggerInterface $logger)
    {
        $this->appRootDir = $appRootDir;
        $this->sites = $sites;
        $this->logger = $logger;
    }

    public function load($resource, $type = null)
    {
        if (true === $this->loaded) {
            throw new \RuntimeException('Do not add this loader twice');
        }
        
        // create an empty route collection
        $collection = new RouteCollection();

        // ensure that we have an array
        if (is_array($this->sites)) {

            // as we have an array we can itterate
            foreach ($this->sites as $site) {

                $resource = $this->appRootDir . '/config/routing_' . $site . '.yml';

                // if we do not have the file
                if (false === file_exists($resource)) {

                    // flag the error
                    $this->logger->err(sprintf("DLRouteLoader - routing file %s is does not exist or is not readable", $resource));
                    continue;
                }

                // 
                $this->logger->info(sprintf("DLRouteLoader - loading additional routing file %s", $resource));

                $type = 'yaml';
                $importedRoutes = $this->import($resource, $type);

                $collection->addCollection($importedRoutes);
            }
        }

        return $collection;
    }

    /**
     * 
     */
    public function supports($resource, $type = null)
    {
        return 'extra' === $type;
    }

}
