DL Routing
=========

This bundle allows additional routing files to be added which can then be turned on or off in the parameters file.

Installation
----

AppKernal.php
-----
Add bundle initialisation code
```
new DL\RoutingBundle\DLRoutingBundle()
```
config.yml
------
Add the following to your config
```
dl_routing:
    extra: %dl.routing.extra%
```

####parameters.yml or parameters_default.yml
Additional config is required in the parameters.yml file.
This is an array of additional routing files which should be turned on for the site.
```
dl.routing.extra:
    - "www"
    - "us"

```

Additional Routing Files
---

Additional routing files can then be added in the **/app/config/** folder of your application.

The names should match the corresponding items in the parameter array eg. **routing_us.yml** and **routing_gb.yml** should be added.

routing.yml
---
The following should be added to your main **routing.yml** file.
```
DLRoutingBundle:
    resource: .
    type: extra
```